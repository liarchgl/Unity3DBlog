# RectTransform

## Anchors

规定基本显示区域，即[Anchor Presets](#anchor-presets)的边框
参数乘以父物体的[Anchor Presets](#anchor-presets)规定出的最终长宽

## Pivot

中心，这一点的位置处于[Anchor Presets](#anchor-presets)规定的位置，如果规定了的话
参数乘以父物体的[Anchor Presets](#anchor-presets)规定出的最终长宽

## Anchor Presets

根据[Anchors](#anchors)规定的基本区域以不同方式规定出最终的显示区域